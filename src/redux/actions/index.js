import {
    SET_EMAIL,
    SET_ID,
} from './types'

export const setEmail = email => ({
    type: SET_EMAIL,
    email
})

export const setId = id => ({
    type: SET_ID,
    id
})

