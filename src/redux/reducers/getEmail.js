import {SET_EMAIL} from '../actions/types'

const initialState = {
    userEmail:"",
}

const getEmail = (state = initialState , action={})=>{
    switch (action.type){
        case SET_EMAIL :
            const {email} = action
            return {
                userEmail : email
            }
            break;
        default : return state
    }
}

export default getEmail