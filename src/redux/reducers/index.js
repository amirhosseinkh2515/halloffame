import { combineReducers } from 'redux';
import getId from './getId'
import getEmail from './getEmail'



const reducers = combineReducers({
    getEmail,
    getId,
})

export default reducers