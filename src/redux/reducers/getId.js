import {SET_ID} from '../actions/types'

const initialState = {
    userId:"",
}

const getId = (state = initialState , action={})=>{
    switch (action.type){
        case SET_ID :
            const {id} = action
            return {
                userId :id
            }
            break;
        default : return state
    }
}

export default getId