import React, { useEffect, useState } from 'react'
import FameList from '../components/FameList'
import { baseURL } from '../components/DefaultConsts'
import { setEmail } from './../redux/actions'
import { connect } from 'react-redux'
import { Spinner } from 'react-bootstrap'

const Home = (props) => {
    const [data, setData] = useState([]);
    const [showSpinnerCon, setShowSpinnerCon] = useState(true)
    useEffect(() => {
        const fetchData = async () => {
            const result = await fetch(`${baseURL}/fames?guest=true`, {
                method: "GET",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                },
            });
            let firstResponse = await result;
            if (firstResponse.status == 200) {
                const json = await firstResponse.json()
                setData(json.data.list)
                setShowSpinnerCon(false)
            }
            else {
                alert("error")
            }


        };
        fetchData();
    }, [props]);


    const renderFameList = () => {
        return (
            data.map((item, index) => <FameList
                key={index} index={index} item={item} />)
        )
    }
    const LogOut = () => {
        if (window.confirm("Are You Sure You Want to Log Out")) {
            props.setEmail("")
            window.location = "#/Login"

        } else {
            alert("canceled!!!")
        }
    };

    return (
        <div className="homeMain">
            <div className="homeTopNav">
                <div className="homeTopNavInner">
                    <p className="defaultTextBlackLarge">
                        {"Choose Your Fame"}
                    </p>
                </div>

            </div>
            <div className="searchMiddleMainCon">
                <Spinner
                    className="spinner"
                    variant="primary"
                    style={{  display: showSpinnerCon ? "flex" : "none" }}
                    animation="border" role="status">
                </Spinner>
                {renderFameList()}
            </div>
            <button onClick={LogOut}
                className="ButtLogginCon" >
                <p className="enterTextWhite">{"Log Out"}</p>
            </button>
        </div>
    )
}
const mapDispatchToProps = dispatch => {
    return {
        setEmail: mail => {
            dispatch(setEmail(mail))
        }
    }
}
export default connect(null, mapDispatchToProps)(Home)

