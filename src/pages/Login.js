import React, { useState } from 'react'
import { connect } from 'react-redux'
import { setEmail } from './../redux/actions'

const Login = (props) => {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const onSubmit = () =>{
        if(email.length >= 10 && password !== ""){
            alert("You Have Successfully Registered")
            props.setEmail(email)
            window.location = "#/Home"
        }
        else if(email.length >= 10 && password == ""){
            alert("Password can not be empty")
        }
        else {
            alert("Please enter a valid email")
        }
    }
    return (
        <div className="loginMainContainer">
            <form className="LogininputMainCon">
                <input
                    className="inputText"
                    onChange={e => setEmail(e.target.value)}
                    placeholder={"Please Enter Your Email"}
                />
                <input
                    className="inputText"
                    onChange={e => setPassword(e.target.value)}
                    placeholder={"Please Enter Your Password"}
                    type="password"
                />
            </form>
            <button onClick={onSubmit}
                className="ButtLogginCon">
                <p className="enterTextWhite">{"Submit"}</p>
            </button>
        </div>
    )
}
const mapDispatchToProps = dispatch => {
    return {
        setEmail: email => {
            dispatch(setEmail(email))
        }
    }
}
export default connect(null, mapDispatchToProps)(Login)
