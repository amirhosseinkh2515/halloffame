import React, { Component } from 'react'
import { connect } from 'react-redux'
import splashScreenImg from '../images/splashScreenImg.png'
import { Redirect } from "react-router-dom";



class SplashScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            renderSplashscreen: true,

        }
         this.checkToken(this.props) 
    }
    checkToken(props) {
        setTimeout(()=>{
            console.log("TRUE?", props.emailRecived !== "")
            if(props.emailRecived,"props.emailRecived"){
                this.setState({
                    renderSplashscreen : false
                })
            }
        },1000)
    }
    render() {
        if (this.state.renderSplashscreen)
            return (
                <div className="SplashScreenCon" >
                    <img className="SplashScreenImg" src={splashScreenImg} />
                </div>
            )
        else if (this.props.emailRecived) {
            return (
                <Redirect to='/Home' />
            )
        }
        else {
            return (
                <Redirect to='/Login' />
            )
        }
    }
}
const mapStateToProps = state => ({ emailRecived: state.getEmail.userEmail })
export default connect(mapStateToProps, null)(SplashScreen)

