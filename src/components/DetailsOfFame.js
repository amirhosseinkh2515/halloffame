import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { baseURL } from '../components/DefaultConsts'
import home_unselected from '../images/home_unselected.png'
import { Spinner } from 'react-bootstrap'

const DetailsOfFame = (props) => {
    const [data, setData] = useState([]);
    const [error, setError] = useState("");
    const [showSpinnerCon, setShowSpinnerCon] = useState(true)
    useEffect(() => {
        const fetchData = async () => {
            try {
                const result = await fetch(`${baseURL}/fames/${props.userId}?guest=true`, {
                    method: "GET",
                    headers: {
                        "Accept": "application/json",
                        "Content-Type": "application/json",
                    },
                });
                let firstResponse = await result;
                if (firstResponse.status == 200) {
                    const json = await firstResponse.json()
                    setData(json.data)
                    setShowSpinnerCon(false)
                }
                else {
                    const json = await firstResponse.json()
                    alert(json.error)
                    setShowSpinnerCon(false)
                }
            } catch (error) {
                setError("error")
            }

        };
        fetchData();
    }, [props]);
    if (error == "") {
        return (
            <div >
                <div className="homeTopNav">
                    <div className="homeTopNavInner">
                        <p className="defaultTextBlackLarge">
                            {data.name}
                        </p>
                    </div>
                </div>

                <div className="detailMainCon">
                    <div
                        onClick={() => window.history.back()}
                        className="homeIcon">
                        <img
                            className="SplashScreenImg"
                            src={home_unselected} />
                    </div>
                    <div className="detailImgCon">
                        <img
                            className="SplashScreenImg"
                            src={data.image} />
                    </div>
                    <Spinner
                        variant="primary"
                        style={{ display: showSpinnerCon ? "flex" : "none" }}
                        animation="border" role="status">
                    </Spinner>
                    <div className="detailInterCon">
                        <p className="defaultTextBlackLarge">{data.dob}</p>
                    </div>
                </div>
            </div>
        )
    }
    else {
        return (
            <div className="homeTopNav">
                <div onClick={() => window.history.back()} className="homeTopNavInner">
                    <p className="defaultTextBlackLarge">
                        You Are Ofline!!! Click To Get Back
                     </p>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => ({ userId: state.getId.userId })
export default connect(mapStateToProps, null)(DetailsOfFame)

