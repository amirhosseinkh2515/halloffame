import React, { useState, useEffect } from 'react'
import { baseURL } from '../components/DefaultConsts'
import { setId } from './../redux/actions'
import { connect } from 'react-redux'

const FameList = (props) => {
    const item = props.item
    const sendurl = () => {
        props.setId(item.id)
        window.location = "#/DetailsOfFame"
    }

    return (
        <div 
            onClick={sendurl}
            className="searchMiddleInerCon">
            <div className="searchMiddleInerPicCon">
                <div className="searchPicCon">
                    <img className="searchProfileImg" src={item.image} />
                </div>
            </div>
            <div className="searchMiddleInerLargeCon">
                <p className="defaultTextBlackLarge">{item.name}</p>
            </div>
            <div className="searchMiddleInerLargeCon">
                <p className="defaultTextBlackLarge">{item.dob}</p>
            </div>
        </div>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        setId: id => {
            dispatch(setId(id))
        }
    }
}
export default connect(null, mapDispatchToProps)(FameList)

