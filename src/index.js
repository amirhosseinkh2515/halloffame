import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import store from './redux/store'
import { Provider } from 'react-redux';
import { BrowserRouter, HashRouter } from 'react-router-dom';

ReactDOM.render(
    <BrowserRouter>
        <HashRouter>
            <Provider store={store}>
                <App />
            </Provider>
        </HashRouter>
    </BrowserRouter>
    , document.getElementById('root'));


serviceWorker.register();
