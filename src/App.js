import React from 'react'
import './css/style.css';
import 'bootstrap/dist/css/bootstrap.css';
import { Route, Switch } from 'react-router-dom';
import SplashScreen from './pages/SplashScreen'
import Login from './pages/Login'
import Home from './pages/Home'
import DetailsOfFame from './components/DetailsOfFame'


const App = () => {
    return (
        <div className="container">
            <Switch className="row">
                <Route exact path="/" component={SplashScreen} />
                <Route path="/Login" component={Login} />
                <Route path="/Home" component={Home} />
                <Route path="/DetailsOfFame" component={DetailsOfFame} />
            </Switch>
        </div>
    )

}

export default App